import json
import requests
import csv
from datetime import datetime

data_e_hora_atuais = datetime.now()
urlString = "https://data.messari.io/api/v2/assets?fields=symbol,metrics/market_data/price_usd,metrics/market_data/volume_last_24_hours,metrics/market_data/percent_change_usd_last_24_hours,metrics/roi_data/percent_change_last_1_week,metrics/roi_data/percent_change_last_1_month,metrics/roi_data/percent_change_last_3_months,metrics/roi_data/percent_change_last_1_year,metrics/risk_metrics/sharpe_ratios,metrics/risk_metrics/volatility_stats&limit=500"


def extractData(coinData):
    symbol = coinData['symbol']
    price_usdt = coinData['metrics']['market_data']['price_usd']
    volume_last_24hours = coinData['metrics']['market_data']['volume_last_24_hours']
    percent_change_usd_last_24_hours = coinData['metrics']['market_data']['percent_change_usd_last_24_hours']

    percent_change_last_1_week = coinData['metrics']['roi_data']['percent_change_last_1_week']
    percent_change_last_1_month = coinData['metrics']['roi_data']['percent_change_last_1_month']
    percent_change_last_3_months = coinData['metrics']['roi_data']['percent_change_last_3_months']
    percent_change_last_1_year = coinData['metrics']['roi_data']['percent_change_last_1_year']

    sharpe_last_30_days = coinData['metrics']['risk_metrics']['sharpe_ratios']['last_30_days']
    sharpe_last_90_days = coinData['metrics']['risk_metrics']['sharpe_ratios']['last_90_days']
    sharpe_last_1_year = coinData['metrics']['risk_metrics']['sharpe_ratios']['last_1_year']
    sharpe_last_3_years = coinData['metrics']['risk_metrics']['sharpe_ratios']['last_3_years']

    volatility_last_30_days = coinData['metrics']['risk_metrics']['volatility_stats']['volatility_last_30_days']
    volatility_last_90_days = coinData['metrics']['risk_metrics']['volatility_stats']['volatility_last_90_days']
    volatility_last_1_year = coinData['metrics']['risk_metrics']['volatility_stats']['volatility_last_1_year']
    volatility_last_3_years = coinData['metrics']['risk_metrics']['volatility_stats']['volatility_last_3_years']

    coinInfo = []
    if symbol is not None:
        vldtSymbol = symbol.strip()
        if len(vldtSymbol) > 0:
            if price_usdt is not None:
                coinInfo.append(symbol)
                coinInfo.append(price_usdt)
                coinInfo.append(volume_last_24hours)
                coinInfo.append(percent_change_usd_last_24_hours)
                coinInfo.append(percent_change_last_1_week)
                coinInfo.append(percent_change_last_1_month)
                coinInfo.append(percent_change_last_3_months)
                coinInfo.append(percent_change_last_1_year)
                coinInfo.append(sharpe_last_30_days)
                coinInfo.append(sharpe_last_90_days)
                coinInfo.append(sharpe_last_1_year)
                coinInfo.append(sharpe_last_3_years)
                coinInfo.append(volatility_last_30_days)
                coinInfo.append(volatility_last_90_days)
                coinInfo.append(volatility_last_1_year)
                coinInfo.append(volatility_last_3_years)

    return coinInfo


def generatecoincsv(coins):
    print('creating csv file')
    headerCoins = ['symbol', 'price_usdt', 'volume_last_24hours', 'percent_change_usd_last_24_hours',
                   'percent_change_last_1_week', 'percent_change_last_1_month', 'percent_change_last_3_months',
                   'percent_change_last_1_year', 'sharpe_last_30_days', 'sharpe_last_90_days', 'sharpe_last_1_year',
                   'sharpe_last_3_years', 'volatility_last_30_days', 'volatility_last_90_days',
                   'volatility_last_1_year',
                   'volatility_last_3_years']

    with open('coins.csv', 'w', newline='') as f:
        write = csv.writer(f)
        write.writerow(headerCoins)
        write.writerows(coins)


def generateexeclog():
    print('creating log file')
    with open('log.csv', 'a', newline='') as f:
        write = csv.writer(f)
        date = []
        date.append(data_e_hora_atuais.strftime('%d/%m/%Y %H:%M'))
        write.writerow(date)
    print('all files were updated')

def callapi():
    status = 200
    cont = 1
    coinsresponse = []
    coins = []

    while status == 200:
        url = urlString + "&page=" + str(cont)
        response = requests.get(url)
        status = response.status_code
        if response.status_code == 200:
            coinresponse = json.loads(response.content)
            coinsData = coinresponse['data']
            for coinData in coinsData:
                coin = extractData(coinData)
                if len(coin) > 14:
                    coins.append(coin)

        cont = cont + 1
    generatecoincsv(coins)
    generateexeclog()


