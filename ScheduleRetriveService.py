from apscheduler.schedulers.blocking import BlockingScheduler

from RetriveService import callapi

sched = BlockingScheduler()

callapi()
@sched.scheduled_job('interval', minutes=11)
def timed_job():
    print('This job is run every 11 minutes.')
    callapi()


sched.start()
